<form id="update_book" method="post">
                    <p>
                        <label class="label" for="title">Title</label>
                        <input v-model="selectedBook.title" class="input" type="text" name="title"/>
                    </p>
                    <p>
                        <label class="label" for="description">Description</label>
                        <input v-model="selectedBook.description" class="input" type="text" name="description"/>
                    </p>
                    <p>
                        <label class="label" for="year_published">Year Published</label>
                        <input v-model="selectedBook.year_published" class="input" type="text" name="year_published"/>
                    </p>
                    <p>
                        <label class="label" for="num_pages">Number of Pages</label>
                        <input v-model="selectedBook.num_pages" class="input" type="text" name="num_pages"/>
                    </p>
                    <p>
                        <label class="label" for="price">Price</label>
                        <input v-model="selectedBook.price" class="input" type="text" name="price"/>
                    </p>
                    <p>
                        <label class="label" for="author_id">Author Id</label>
                        <input v-model="selectedBook.author_id" class="input" type="text" name="author_id"/>
                    </p>
                    <p>
                        <label class="label" for="genre_id">Genre Id</label>
                        <input v-model="selectedBook.genre_id" class="input" type="text" name="genre_id"/>
                    </p>
                    <p>
                        <label class="label" for="format_id">Format Id</label>
                        <input v-model="selectedBook.format_id" class="input" type="text" name="format_id"/>
                    </p>
                    <p>
                        <label class="label" for="publisher_id">Publisher Id</label>
                        <input v-model="selectedBook.publisher_id" class="input" type="text" name="publisher_id"/>
                    </p>
                    <p>
                        <label class="label" for="in_print">In Print</label>
                        <input v-model="selectedBook.in_print" class="input" type="text" name="in_print"/>
                    </p>
                    <p>
                        <button class="button is-primary" type="button"
                                v-on:click="updateBook">Update </button>&nbsp;
                        <button class="button" @click="closeBook">Cancel</button>
                    </p>
                </form>