<?php

/**
     * @file Validator.php
     * @desc Class for validations
     * @updated_at  2020-09-30 
*/

namespace App\Lib;

use DateTime;

class Validator
{
    private $post = [];
    private $errors = [];

    /**
     * [__construct function invoked on object created outside class]
     * @param array $array [$_POST data]
     */
    public function __construct($array)
    {
        $this->post=$array;
    }

    /**
     * [label - function to change the form names into presentable data]
     * @param  string $field_name [names of form elements]
     * @return string             [presentable data for output]
     */
    private function label($field_name)
    {
        return ucwords(str_replace('_', ' ', $field_name));
    }

    /**
     * [required - function to check that user has 
     * entered values in all required fields]
     * @param  string $required [names of required form elements]
     */
    public function required($required) //called signature of the function
    {
        foreach($required as $key) {
            if(strlen($this->post[$key])==0) {
                $this->errors[$key][] = $this->label($key) . ' is required';
            } 
        }
    }

    /**
     * [email_check - check for valid email format]
     * @param  string $field [name of email field in the form]
     */
    public function email_check($field)
    {
        $email=$this->post[$field];
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $print = $this->label($field);
            $this->errors[$field][] = $print . " is not a valid email address";
        } else {
            $pattern='/^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}$/';
            if(preg_match($pattern, $email, $matches)===0) {
                $print1 = $this->label($field);
                $this->errors[$field][] = $print1 . " is not a valid email format.";
            }
            if(preg_match($pattern, $email, $matches)===false) {
                $this->errors[$field][] = $this->label($field) . " got some error";
            }
        }
    }

    /**
     * [match_val - comparing two values to be same]
     * @param  string $key1 [name of field in form]
     * @param  string $key2 [other name of fiedl in form]
     */
    public function match_val($key1,$key2)
    {
        $pwd = $this->post[$key1];
        $pwd_confirm = $this->post[$key2];
        if($pwd!=$pwd_confirm) {
            $this->errors[$key1][] = $this->label($key1) . " does not match";
        }
    }

    /**
     * [length_check - check the minimum and maximum 
     * length of the string as per requirement]
     * @param  string $field [name of field in form]
     * @param  int $min   [minimum length]
     * @param  int $max   [maximum length]
     */
    public function length_check($field,$min,$max)
    {
        $var_length=strlen($this->post[$field]);
        if($var_length<$min || $var_length>$max) {
            $print = $this->label($field);
            $message = " should be minimum {$min} or maximum {$max} character long";
            $this->errors[$field][] = $print . $message;
        }
    }

    /**
     * [validate_number - check for value to be integer 
     * and to be in between minimum and maximum number]
     * @param  string $field [name of field in form]
     * @param  int $min   [minimum value]
     * @param  int $max   [maximum value]
     */
    public function validate_number($field,$min,$max)
    {
        $var=$this->post[$field];
        if(!is_numeric($var)) {
            $this->errors[$field][] = $this->label($field) . " should be a number";
        } elseif($var<$min || $var>$max) {
            $message = " should be of minimum {$min} or maximum {$max} number";
            $this->errors[$field][] = $this->label($field) . $message;
        }
    }

    /**
     * [is_number - check for value to be integer 
     * ]
     * @param  string $field [name of field in form]
     */
    public function is_number($field)
    {
        $var=$this->post[$field];
        if(!is_numeric($var)) {
            $this->errors[$field][] = $this->label($field) . " should be a number";
        } 
    }

    /**
     * [check_number check any number and its length]
     * @param  [string] $field [description]
     * @param  [int] $min   [description]
     * @param  [int] $max   [description]
     */
    public function check_number($field,$min,$max)
    {
        $var=$this->post[$field];
        $var_length=strlen($this->post[$field]);
        if(!is_numeric($var)) {
            $this->errors[$field][] = $this->label($field) . " should be a number";
        } elseif($var_length<$min || $var_length>$max) {
            $print = $this->label($field);
            $message = " should be minimum of {$max} digits";
            $this->errors[$field][] = $print . $message;
        }
    }

    /**
     * [check_post - check for valid postal code]
     * @param  string $field [name of postal code field in form]
     */
    public function check_post($field)
    {
        $pattern = '/^([a-zA-Z]{1}[\d]{1}[a-zA-Z]{1})[- ]?([\d]{1}[a-zA-Z]{1}[\d]{1})$/';
        if(preg_match($pattern, $this->post[$field], $matches)===0) {
            $this->errors[$field][] = $this->label($field) . " is not a valid postal code";
        }
        if(preg_match($pattern, $this->post[$field], $matches)===false) {
            $this->errors[$field][] = $this->label($field) . " got some error";
        }
    }

    /**
     * [check_phone - check for valid phone number (000)-/ 000-/ 0000]
     * @param  string $field [name of phone field in form]
     */
    public function check_phone($field)
    {
        $pattern = '/^\(?[0-9]{3}\)?(\s|\-|\.)\s?[0-9]{3}(\-|\.|\s)[0-9]{4}$/';
        if(preg_match($pattern, $this->post[$field],$matches)===0) {
            $this->errors[$field][] = $this->label($field) . " is not valid.";
        } 
        if(preg_match($pattern, $this->post[$field], $matches)===false) {
            $this->errors[$field][] = $this->label($field) . " got some error";
        }
    }

    /**
     * [check_name - check valid first and last name]
     * @param  string $field [name of field of the form]
     */
    public function check_name($field)
    {
        $pattern = '/^([a-zA-Z\-]+\s?)*$/';
        if(preg_match($pattern, $this->post[$field],$matches)===0) {
            $this->errors[$field][] = $this->label($field) . " is not valid.";
        } 
        if(preg_match($pattern, $this->post[$field], $matches)===false) {
            $this->errors[$field][] = $this->label($field) . " got some error";
        }
    }

    /**
     * [check_password - check for valid format of a password]
     * @param  string $field [name of password field in the form]
     */
    public function check_password($field)
    {
        $pattern='/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/';
        if(preg_match($pattern, $this->post[$field],$matches)===0) {
            $this->errors[$field][] = $this->label($field) . " is invalid in format";
        } 
        if(preg_match($pattern, $this->post[$field], $matches)===false) {
            $this->errors[$field][] = $this->label($field) . " got some error";
        }
    }

    /**
     * [validateCC - Code taken from 
     * stackoverflow.com/questions/174730/what-is-the-best-way-to-validate-a-credit-card-in-php]
     * PHP Code No. 6
     * [validateCC description]
     * @param  [string] $cc_number [description]
     * @param  [string] $field     [description]
     */
    public function validateCC($cc_number, $field) {
        $cc_num = $this->post[$cc_number];

        $type = $this->post[$field];

        if($type=="none") {
            $this->errors[$field][] = $this->label($field) . " should be selected";
            return;
        } elseif($type == "American") {
            $denum = "American Express";
        } elseif($type == "Dinners") {
            $denum = "Diner's Club";
        } elseif($type == "Discover") {
            $denum = "Discover";
        } elseif($type == "Master") {
            $denum = "Master Card";
        } elseif($type == "Visa") {
            $denum = "Visa";
        }

        if($type == "American") {
            $pattern = "/^([34|37]{2})([0-9]{13})$/";//American Express
        if (preg_match($pattern,$cc_num)) {
            $verified = true;
        } else {
            $verified = false;
        }


        } elseif($type == "Dinners") {
            $pattern = "/^([30|36|38]{2})([0-9]{12})$/";//Diner's Club
        if (preg_match($pattern,$cc_num)) {
            $verified = true;
        } else {
            $verified = false;
        }


        } elseif($type == "Discover") {
            $pattern = "/^([6011]{4})([0-9]{12})$/";//Discover Card
        if (preg_match($pattern,$cc_num)) {
            $verified = true;
        } else {
            $verified = false;
        }


        } elseif($type == "Master") {
            $pattern = "/^([51|52|53|54|55]{2})([0-9]{14})$/";//Mastercard
        if (preg_match($pattern,$cc_num)) {
            $verified = true;
        } else {
            $verified = false;
        }


        } elseif($type == "Visa") {
            $pattern = "/^([4]{1})([0-9]{12,15})$/";//Visa
        if (preg_match($pattern,$cc_num)) {
            $verified = true;
        } else {
            $verified = false;
        }

        }

        if($verified == false) {
            //Do something here in case the validation fails
            $this->errors[$cc_number][] = $this->label($cc_number) . " is not valid"; 
        } 
    }

    /**
     * [checkExpiry Code taken from
     * stackoverflow.com/questions/62098257/validating-expiration-date-in-php]
     * @param  [string] $field [description]
     */
    public function checkExpiry($field)
    {
        $expiryDt = $this->post[$field] ?? '';
        if($expiryDt != '')
        {
            $userInput = substr($expiryDt, 0,2) . '-' . substr($expiryDt, 2,3) ?? ''; // Example User input
            if($userInput != '')
            {           
                     
                $cardDate = DateTime::createFromFormat('m-y', $userInput);

                $currentDate = new DateTime('now');
                $interval = $currentDate->diff($cardDate);

                if ( $interval->invert == 1 ) {

                    // Expired
                    $this->errors[$field][] = $this->label($field) . " is not valid";
                }                
            }
        }
    }

    /**
     * [checkSelected function to check if the dropdown is selected]
     * @param  [string] $field [select field name]
     */
    public function checkSelected($field)
    {
        $time = $this->post[$field];
        if($time == "none") {
            $this->errors[$field][] = $this->label($field) . " should be selected.";
        }
    }

    /**
     * [checkPrice function to check price not to be Zero]
     * @param  [string] $field [Price field name]
     */
    public function checkPrice($field)
    {
        $price = $this->post[$field];
        if($price == 0 || $price == '') {
            $this->errors[$field][] = $this->label($field) . " should not be 0.";
        }
    }

    /**
     * [errors - function to return $errors array values]
     * @return array $errors [array containing errors]
     */
    public function errors()
    {
        return $this->errors;
    }

    /**
     * [post - function to return $post array values]
     * @return array $post [array containing $_POST values]
     */
    public function post()
    {
        return $this->post;
    }
}