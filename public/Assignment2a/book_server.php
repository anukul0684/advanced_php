<?php

require __DIR__ . '/Validator.php';

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);
$dbh = new PDO('sqlite:database.sqlite');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


if('POST' === $_SERVER['REQUEST_METHOD']) {

    if(empty($_POST)) {
            $json = file_get_contents("php://input");
            $post = (array) json_decode($json);
    } else {
        $post=$_POST;
    }
    
    //var_dump($post);
    //die;
    
    $v = new Validator($post);

    $required_field=array(
                  'title',
                  'description',
                  'num_pages',
                  'price',
                  'year_published',
                  'in_print',
                  'author_id',
                  'genre_id',
                  'format_id',
                  'publisher_id');

    $v->required($required_field);

    $v->check_name('title');
    $v->length_check('description',5,255);
    $v->is_number('num_pages');
    $v->is_number('price');
    $v->is_number('year_published');
    $v->validate_number('in_print',0,1);
    $v->validate_number('author_id',1,12);
    $v->validate_number('genre_id',1,6);
    $v->validate_number('format_id',1,3);
    $v->validate_number('publisher_id',1,7);

    $errors=$v->errors();
    $post=$v->post();

    if(empty($errors)) {
        $query = "UPDATE book 
                    SET 
                    title = :title,
                    description = :description,
                    num_pages = :num_pages,
                    price = :price,
                    year_published = :year_published,
                    in_print = :in_print,
                    author_id = :author_id,
                    genre_id = :genre_id,
                    format_id = :format_id,
                    publisher_id = :publisher_id
                    WHERE book_id=:book_id";
        $stmt = $dbh->prepare($query);
        $params = array(
            ':title' => $post['title'],
            ':description' => $post['description'],
            ':num_pages' => $post['num_pages'],
            ':price' => $post['price'],
            ':year_published' => $post['year_published'],
            ':in_print' => $post['in_print'],
            ':author_id' => $post['author_id'],
            ':genre_id' => $post['genre_id'],
            ':format_id' => $post['format_id'],
            ':publisher_id' => $post['publisher_id']
        );
        $stmt->execute($params);
        $id = $dbh->lastInsertId();
        $results = ['id' => $id, 'success' => 'Updated successfully'];
    } else {
        $errors['error'] = 'Some errors found';
        $results = $errors;
    }
} elseif(!empty($_GET['book_id'])) {
     $query = 'SELECT book.*, 
        genre.name as genre,
        format.name as format,
        author.name as author,
        publisher.name as publisher
        FROM book 
        JOIN author USING(author_id)
        JOIN format USING(format_id)
        JOIN genre USING(genre_id)
        JOIN publisher USING(publisher_id)
        WHERE book_id = :book_id';
    $stmt = $dbh->prepare($query);
    $params = array(':book_id' => (int) $_GET['book_id']);
    $stmt->execute($params);
    $results = $stmt->fetch(PDO::FETCH_ASSOC);
} elseif(!empty($_GET['genre_id'])) {
   $query = 'SELECT book.*, 
        genre.name as genre,
        format.name as format,
        author.name as author,
        publisher.name as publisher
        FROM book 
        JOIN author USING(author_id)
        JOIN format USING(format_id)
        JOIN genre USING(genre_id)
        JOIN publisher USING(publisher_id)
        WHERE genre_id = :genre_id';
    $stmt = $dbh->prepare($query);
    $params = array(':genre_id' => (int) $_GET['genre_id']);
    $stmt->execute($params);
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
} elseif(!empty($_GET['format_id'])) {
    $query = 'SELECT book.*, 
        genre.name as genre,
        format.name as format,
        author.name as author,
        publisher.name as publisher
        FROM book 
        JOIN author USING(author_id)
        JOIN format USING(format_id)
        JOIN genre USING(genre_id)
        JOIN publisher USING(publisher_id)
        WHERE format_id = :format_id';
    $stmt = $dbh->prepare($query);
    $params = array(':format_id' => (int) $_GET['format_id']);
    $stmt->execute($params);
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
} elseif(!empty($_GET['publisher_id'])) {
    $query = 'SELECT book.*, 
        genre.name as genre,
        format.name as format,
        author.name as author,
        publisher.name as publisher
        FROM book 
        JOIN author USING(author_id)
        JOIN format USING(format_id)
        JOIN genre USING(genre_id)
        JOIN publisher USING(publisher_id)
        WHERE publisher_id = :publisher_id';
    $stmt = $dbh->prepare($query);
    $params = array(':publisher_id' => (int) $_GET['publisher_id']);
    $stmt->execute($params);
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
} elseif(!empty($_GET['author_id'])) {
    $query = 'SELECT book.*, 
        genre.name as genre,
        format.name as format,
        author.name as author,
        publisher.name as publisher
        FROM book 
        JOIN author USING(author_id)
        JOIN format USING(format_id)
        JOIN genre USING(genre_id)
        JOIN publisher USING(publisher_id)
        WHERE author_id = :author_id';
    $stmt = $dbh->prepare($query);
    $params = array(':author_id' => (int) $_GET['author_id']);
    $stmt->execute($params);
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
}  else {
     $query = 'SELECT book.*, 
        genre.name as genre,
        format.name as format,
        author.name as author,
        publisher.name as publisher
        FROM book 
        JOIN author USING(author_id)
        JOIN format USING(format_id)
        JOIN genre USING(genre_id)
        JOIN publisher USING(publisher_id)';
    $stmt = $dbh->query($query);
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
}
header('Content-Type: application/json');
echo json_encode($results);