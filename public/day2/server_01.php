<?php

$user = array(
    'first' => 'Anu',
    'last' => 'Kulshrestha',
    'email' => 'anu@gmail.com'
);


$json = json_encode($user);

//content-type: text/html
//
header('Content-Type: application/json');
echo $json;