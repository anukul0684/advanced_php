<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sending the POST data with XHR</title>
    <meta charset="utf-8" />
    <meta name = "viewport"
    content="width=device-width,initial-scale=1.0" />    
    <style></style>
    <script>
        
        var xhr = new XMLHttpRequest();

        // var data = [];
        // data['name'] = 'Anu Kulshrestha';
        // data['email'] = 'anu.kul@gmail.com';
        // data['phone'] = '204.599.0000';

        var data = 'first=Anu&last=Kulshrestha&email=anu@gmail.com';

        xhr.open('POST', 'server_05.php');

        xhr.responseType = 'json';

        //application/x-www-form-urlencoded
        //<form enctype=
        
        //xhr set request ype header
        //Tells PHP, this is as if it came from a form
        //make it available in $_POST
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        xhr.onreadystatechange=function() {
            if(xhr.readyState===4 && xhr.status === 200){
                console.log(xhr.response);
            }
        }

        xhr.send(encodeURI(data));
    </script>
</head>
<body>
    <h1>Sending the POST data with XHR</h1>
    <p>see the console for the output</p>
</body>
</html>