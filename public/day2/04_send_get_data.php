<!DOCTYPE html>
<html lang="en">
<head>
    <title>Send Get Data</title>
    <meta charset="utf-8" />
    <meta name = "viewport"
    content="width=device-width,initial-scale=1.0" />    
    <style></style>
    <script>
        //create xhr object
        var xhr = new XMLHttpRequest();

        //create the data string
        var data = 'first=Anu&last=Kulshrestha&email=anu@gmail.com';

        //open request to url, encode data as URI friendly string
        xhr.open('GET', 'server_04.php?' + encodeURI(data));
        //xhr.open('GET', 'server_04.php?');
        
        //set response type
        xhr.responseType = 'json';

        //add onreadystatechange handler(console log the response)
        xhr.onreadystatechange = function() {
            if(xhr.readyState===4 && this.status == 200) {
                console.log(this.response);
            }
        }

        //send the request
        //xhr.send(encodeURI(data)); // for GET request it will be treated as null irrespective of what we try to send
        xhr.send(null);
    </script>
</head>
<body>
    <h1>Send Get Data with XMLHttpRequest Object</h1>
    <p>See console for the output </p>
    
</body>
</html>