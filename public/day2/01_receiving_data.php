<?php
$title = 'Receiving Data with Ajax';

?><!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$title?></title>
    <meta charset="utf-8" />
    <meta name = "viewport"
    content="width=device-width,initial-scale=1.0" />    
    <style></style>
    <script>
        var xhr = new XMLHttpRequest();
        xhr.open('GET','server_01.php');
        xhr.responseType = 'json';
        xhr.onreadystatechange = function() {
            if(xhr.readyState === 4 && this.status == 200) {
                console.log(this.response);
                
            }
        }

        xhr.send(null);
    </script>
</head>
<body>
    <h1><?=$title?></h1>
    <div id="output">
        <p>See console for output</p>
    </div>
</body>
</html>