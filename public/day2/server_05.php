<?php

header('Content-Type: application/json');

if('POST' !== $_SERVER['REQUEST_METHOD']) {
    $error = ['error' => 'Unsupported request method'];

    echo json_encode($error);
    die;
}

//we could do something here with the data... eg: insert in into a database
//
//
//but for now, let's just echo the data that was sent to us through POST request
//Method1
//$str = file_get_contents('php://input');
//echo $str;

//parse_str($str,$data);
//echo json_encode($data);

//Method2
echo json_encode($_POST);