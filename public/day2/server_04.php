<?php

header('Content-Type: application/json');

if(empty($_GET)) {
    $error = ['error'=>'Please add data to your GET request'];
    echo json_encode($error);
    die;
}

//Do something with the data
//
//

//send data back as JSON endoded 
echo json_encode($_GET);