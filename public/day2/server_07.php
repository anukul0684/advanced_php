<?php

header('Content-Type: application/json');

if('POST' !== $_SERVER['REQUEST_METHOD']) {
    $error = ['error' => 'Unsupported request method'];

    echo json_encode($error);
    die;
}

//we could do something here with the data... eg: insert in into a database
//
//
//but for now, let's just echo the data that was sent to us through POST request
//Method1
//$str = file_get_contents('php://input');
//echo $str;

//parse_str($str,$data);
//echo json_encode($data);

//Method2
//echo json_encode($_POST);
//
//Get the JSON string from the POST submission
$json = file_get_contents('php://input');

//convert JSON string to PHP object
$post = json_decode($json);

//we could do something here with the data... eg: insert in into a database
//
//
//But for now let us just return to prove it that we received it
//output the POST submission as a string to prove that we received it.
echo json_encode($post);