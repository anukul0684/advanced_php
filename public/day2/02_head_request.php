<!DOCTYPE html>
<html lang="en">
<head>
    <title>Head Request</title>
    <meta charset="utf-8" />
    <meta name = "viewport"
    content="width=device-width,initial-scale=1.0" />    
    <style></style>
    <script>
        var xhr = new XMLHttpRequest();

        //HEAD requests are for information about the type of headers
        //a resource would return... nothing else
        xhr.open('HEAD', 'server_01.php');

        xhr.onreadystatechange = function() {
            if(this.readyState === 4 && xhr.status === 200) {
                console.log(this.response);
                console.log(this.getAllResponseHeaders());
            }
        }

        xhr.send();
    </script>
</head>
<body>
    <h1>Head Request</h1>
    <div id="content">
        <p>See console for the output</p>
    </div>
</body>
</html>