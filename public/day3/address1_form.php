<!DOCTYPE html>
<html lang="en">
<head>
    <title>Address Book</title>
    <meta charset="utf-8" />
    <meta name = "viewport"
    content="width=device-width,initial-scale=1.0" />   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css" />
    
    <style>
        body{
            margin-top: 50px;
        }

        h3{
            text-align: center;
        }

        fieldset{
            border: 1px solid #900;
            border-radius: 10px;
            padding:15px;
            box-sizing: border-box;
        }

        label.required:before{
            content: '*';
            color: #900;
        }
        
        
    </style>
    <script>

        window.onload = function()
        {
            handleForm();
        }
        
        /**
         * Stack Overflow - escape function for Javascript
         * @param  {string} text 
         * @return {string} sanitized text
         */
        function e(text) {
            return text
              .replace(/&/g, "&amp;")
              .replace(/</g, "&lt;")
              .replace(/>/g, "&gt;")
              .replace(/"/g, "&quot;")
              .replace(/'/g, "&#039;");
        }   

        function handleForm()
        {
            document.getElementById('add_person').addEventListener('submit',function(e){
                e.preventDefault();
                // if(this.first_name.value.length === 0 || this.last_name.value.length ===0) {
                //     alert('First Name and Last Name are required Fields!');
                //     return;
                // }

                var data ={};

                data.first_name = this.first_name.value;
                data.last_name = this.last_name.value;
                data.street = this.street.value;
                data.city = this.city.value;
                data.country = this.country.value;
                data.postal_code = this.postal_code.value;
                data.phone = this.phone.value;
                data.email1 = this.email1.value;
                data.email2 = this.email2.value;
                
                saveUser(data);
            });
        }

        function saveUser(data)
        {
            url='address1_book_server.php';
            var params = {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json"
                }
            };

            fetch(url,params)
                .then(function(response){
                    return response.json();
                })
                .then(function(data){
                    console.log(data);
                    outputList(data);
                })
                .then(function(error){
                    console.log(error);
                });
        }

        function outputList(list)
        {
            //do code to dynamically create HTML output to div
            
            var html = `<ul class="list-group">`
            for (var i=0;i<list.length; i++)
            {
                html += `<li class="list-group-item">` + list[i].first_name 
                        + ' ' + list[i].last_name + '</a> - (#)' + list[i].phone + `</li>`;
            }          
            
            html += `</ul>`;
            
            document.getElementById('list').innerHTML = html;
        }

    </script>
</head>
<body>
    <div class="container">
        <div class = "row">
            <div class="col-sm-6">
                <h1>Add Person</h1>            
                <form class="form" id="add_person">
                    <fieldset>
                        <legend>Address Book</legend>
                        <div class="form-group">
                            <label for="first_name" class="required">First Name</label>
                            <input type="text" class="form-control" name="first_name" id="first_name" />
                        </div>
                        <div class="form-group">
                            <label for="last_name" class="required">Last Name</label>
                            <input type="text" class="form-control" name="last_name" id="last_name" />
                        </div>
                        <div class="form-group">
                            <label for="street">Street</label>
                            <input type="text" class="form-control" name="street" id="street" />
                        </div>                        
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" class="form-control" name="city" id="city" />
                        </div>
                        <div class="form-group">
                            <label for="country">Country</label>
                            <input type="text" class="form-control" name="country" id="country" />
                        </div>
                        <div class="form-group">
                            <label for="postal_code">Postal Code</label>
                            <input type="text" class="form-control" name="postal_code" id="postal_code" />
                        </div>
                        <div class="form-group">
                            <label for="phone">Contact Number</label>
                            <input type="text" class="form-control" name="phone" id="phone" />
                        </div>
                        <div class="form-group">
                            <label for="email1">Email 1</label>
                            <input type="text" class="form-control" name="email1" id="email1" />
                        </div>
                        <div class="form-group">
                            <label for="email2">Email 2</label>
                            <input type="text" class="form-control" name="email2" id="email2" />
                        </div>
                        <div class="form-group">
                            <button type="reset" class="btn btn-danger">Reset</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </fieldset>                    
                </form>                
            </div>  
            <div class="col-sm-6">
                <div id="list">
                </div>
            </div>          
        </div>
    </div>
</body>
</html>