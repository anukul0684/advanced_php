<?php

function main()
{
    header('Conent-Type: application/json');

    //GET no parameters means all results
    //GET id param means one result
    //POST means insert a record
    //PUT means update a record
    //DELETE means delete a record
    try {
        
        if('POST' === $_SERVER['REQUEST_METHOD']) {
            $php_input = file_get_contents("php://input");
            $post = (array) json_decode($php_input);            
            save($post);
            $result = getLatest();

        } elseif('GET' === $_SERVER['REQUEST_METHOD'] && (!(empty($_GET['search_code'])))) {
            $result = getSearch($_GET['search_code']);
        }
        elseif('GET' === $_SERVER['REQUEST_METHOD'] && (!(empty($_GET['user_id'])))) {
            $result = getOne($_GET['user_id']);
        } elseif('GET' === $_SERVER['REQUEST_METHOD'] && empty($_GET)) {
            $result = getAll();
        }
        
        echo json_encode($result);

    } catch(Exception $e) {
        echo json_encode($e->getTrace());
    }
}

// function emails()
// {
//     header('Conent-Type: application/json');
//     $result = getEmails(1);
//     echo json_encode($result);
// }

// function user()
// {
//     header('Conent-Type: application/json');
//     $result = getOne(1);
//     echo json_encode($result);
// }

// Functions used in this file
function getDBH()
{
    $dbh = new PDO('sqlite:address1.sqlite');
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    return $dbh;
}

function getAll()
{
    $dbh = getDBH();
    $query = 'SELECT * FROM users order by first_name asc';
    $stmt = $dbh->query($query);
    return $stmt->fetchAll();
}

function getLatest()
{
    $dbh = getDBH();
    $query = 'SELECT * FROM users order by created_at desc';
    $stmt = $dbh->query($query);
    return $stmt->fetchAll();
}

function getOne($id)
{
    $dbh = getDBH();
    $query = 'SELECT * FROM users where id=:id';
    $stmt = $dbh->prepare($query);
    $params = array(':id'=>$id);
    $stmt->execute($params);    
    $result =  $stmt->fetch();
    $result['emails'] = getEmails($id);
    $result['hash'] = md5($result['emails'][0]['email']); 
    return $result;
}

function getEmails($id)
{
    $dbh = getDBH();
    $query = 'SELECT email FROM emails where user_id=:id';
    $stmt = $dbh->prepare($query);
    $params = array(':id'=>$id);
    $stmt->execute($params);
    return $stmt->fetchAll();    
}

function getSearch($str)
{
    $dbh = getDBH();
    //$str = '%' . $str . '%';
    $query = "SELECT * FROM users where (first_name like '%'||:str||'%' 
                        OR last_name like '%'||:str||'%')                         
                        order by first_name ASC";
    //$stmt->bindValue(':str', (string) $str, PDO::PARAM_STR);    
    $params = array(
                ':str'=>$str);                
    $stmt = $dbh->prepare($query);    
    $stmt->execute($params);    
    $result =  $stmt->fetchAll();    
    return $result;
}

function save($data)
{

    $dbh = getDBH();
    $dbh->beginTransaction();
    $query = "INSERT INTO users
            (first_name, last_name, street, city, postal_code, country, phone) 
            VALUES
            (:first_name, :last_name, :street, :city, :postal_code, :country, :phone)";
    
    $params = array(
        ':first_name'=>$data['first_name'],
        ':last_name'=>$data['last_name'],
        ':street'=>$data['street'],
        ':city'=>$data['city'],
        ':postal_code'=>$data['postal_code'],
        ':country'=>$data['country'],
        ':phone'=>$data['phone']
    );


    $stmt = $dbh->prepare($query);
    $stmt->execute($params);
    $id= $dbh->lastInsertId();

    if(!empty($id)) {

        if(!empty($data['email1']) || !empty($data['email2'])) {
            
            saveEmails($data, $id);    

        }        
    }    

    $dbh->commit();
}

function saveEmails($user, $id)
{    
    $dbh = getDBH();
    $emails = [];
    $emails[] = $user['email1'] ?? '';
    $emails[] = $user['email2'] ?? '';

    $query = "INSERT INTO emails(user_id,email)
                VALUES 
                (:user_id,:email)";
    foreach($emails as $email) {
        
        $stmt = $dbh->prepare($query);
        $params = array(':user_id' => $id, ':email' => $email);
        
        $stmt->execute($params);
        //return $stmt->lastInsertId();
    }       
}

main();
// echo "<hr/>";
// emails();
// echo "<hr/>";
// user();