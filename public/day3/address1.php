<!Doctype html>
<html lang="en">
<head>
    <title>Address Book</title>
    <meta charset="utf-8" />
    <meta name = "viewport"
    content="width=device-width,initial-scale=1.0" />   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css" />
    
    <style>
        body{
            margin-top: 50px;
        }
        h3{
            text-align: center;
        }
        
    </style>
    <script>

        window.onload=function() {
            getAllUsers();
            handleForm();            
        }

        function getAllUsers()
        {
            var xhr = new XMLHttpRequest();
            
            xhr.open('GET','address1_book_server.php');
            xhr.responseType = 'json';
            xhr.onreadystatechange = function() {
                if(xhr.readyState === 4 && xhr.status===200) {
                    console.log(xhr.response);
                    outputList(xhr.response);
                }
            }
            xhr.send(null);
        }

        function outputList(list)
        {
            //do code to dynamically create HTML output to div
            
            var html = `<ul class="list-group">`
            for (var i=0;i<list.length; i++)
            {
                html += `<li class="list-group-item">
                <a href="javascript:;" onclick = "return getOneUser(` + list[i].id + `)">` + list[i].first_name 
                        + ' ' + list[i].last_name + '</a> - (#)' + list[i].phone + `</li>`;
            }          
            
            html += `</ul>`;
            
            document.getElementById('list').innerHTML = html;
        }

        function getOneUser(id)
        {
            var xhr = new XMLHttpRequest();
            var idstring = 'user_id=' + id;
            xhr.open('GET','address1_book_server.php?' + encodeURI(idstring));
            xhr.responseType = 'json';
            xhr.onreadystatechange = function() {
                if(xhr.readyState === 4 && xhr.status===200) {
                    console.log(xhr.response);
                    outputDetail(xhr.response);
                }
            }
            xhr.send(null);
        }

        function outputDetail(details)
        {
            var html = `<div class="card-deck">
                            <div class="card">`;   
            html += `<h3 class="card-title">Details of ` + details.first_name + `</h3>`;
            html += `<img class="card-img-top" src="https://www.gravatar.com/avatar/` 
                    + details.hash + `?s=100" alt="Card image cap"/>`;         
            html += `<div class="card-body">
                        <p class="card-text"><strong>Name</strong>: ` 
                        + details.first_name + ' ' + details.last_name + `</p>`;                             
            html += `<p class="card-text"><strong>Street</strong>: ` + details.street;
            html += `<p class="card-text"><strong>City</strong>: ` + details.city;
            html += `<p class="card-text"><strong>Country</strong>: ` + details.country;
            html += `<p class="card-text"><strong>Phone</strong>: ` + details.phone;
            html += `<p class="card-text"><strong>Emails</strong>:`;
            html += `<ul class="list-group">`;
            for(var i=0;i<details.emails.length;i++)
            {
                html += `<li class="list-group-item">` + details.emails[i].email + `</li>`;
            } 
            html += `</ul></p>`;
            html += `  </div>            
                    </div>
                </div>`;
            document.getElementById('detail').innerHTML = html;
        }

        /**
         * Stack Overflow - escape function for Javascript
         * @param  {string} text 
         * @return {string} sanitized text
         */
        function e(text) {
            return text
              .replace(/&/g, "&amp;")
              .replace(/</g, "&lt;")
              .replace(/>/g, "&gt;")
              .replace(/"/g, "&quot;")
              .replace(/'/g, "&#039;");
        }

        function handleForm()
        {
            console.log(document.getElementById('searchbox').value);
            document.getElementById('searchbox').addEventListener('keyup',function(e){
                //e.preventDefault();
                console.log(this.value);
                var xhr = new XMLHttpRequest();
                var search_string = 'search_code=' + this.value;
                xhr.open('GET','address1_book_server.php?' + encodeURI(search_string));
                xhr.responseType = 'json';
                xhr.onreadystatechange = function() {
                    if(xhr.readyState === 4 && xhr.status===200) {
                        console.log(xhr.response);
                        outputList(xhr.response);
                    }
                }
                xhr.send(null);    
            });                    
        }

        // function handleForm(e,el)
        // {
        //     e.preventDefault();
        //     console.log(el.value);
        //     var xhr = new XMLHttpRequest();
        //     var search_string = 'search_code=' + el.value;
        //     xhr.open('GET','address1_book_server.php?' + encodeURI(search_string));
        //     xhr.responseType = 'json';
        //     xhr.onreadystatechange = function() {
        //         if(xhr.readyState === 4 && xhr.status===200) {
        //             console.log(xhr.response);
        //             outputList(xhr.response);
        //         }
        //     }
        //     xhr.send(null); 
        //     return false;          
        // }


    </script>
</head>
<body>
    <div class="container">
        <div class = "row">
            <div class="col-sm-6">
                <h1>Address Book</h1>            
                <form class="form">
                    <input id="searchbox" class="form-control" name="searchbox" 
                    type="text" placeholder="search..."/>
                </form>
                <div id="list">
                </div>
            </div>
            <div id="detail" class="col-sm-6">

            </div>
        </div>
    </div>
</body>
</html>