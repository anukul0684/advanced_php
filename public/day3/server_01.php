<?php

header('Content-Type: application/json');

$data = new stdClass();

$json = file_get_contents('php://input');

$php_input = json_decode($json);

$data->get = $_GET;
$data->post = $_POST;
$data->php_input = $php_input;
$data->req = $_SERVER['REQUEST_METHOD'];

echo json_encode($data);