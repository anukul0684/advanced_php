
CREATE TABLE users (
    id INTEGER NOT NULL PRIMARY KEY,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    street VARCHAR(255),
    city VARCHAR(255),
    postal_code VARCHAR(10),
    country VARCHAR(255),
    phone VARCHAR(255),
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE emails (
    id INTEGER NOT NULL PRIMARY KEY,
    user_id INTEGER,
    email VARCHAR(255)
);

INSERT INTO users
(id, first_name,last_name,street,city,postal_code,country,phone)
VALUES
(1,'Anu', 'Kulshrestha', '505 Camden Place', 'Winnipeg', 'R3Y 1J4', 'Canada', '204-123-1234'),
(2,'Ankit', 'Kulshrestha', '333 Rutland Street', 'Winnipeg', 'R3N 2JG', 'Canada', '204-123-5432'),
(3,'Anish', 'Kulshrestha', '754 Pembina Highway', 'Winnipeg', 'R3N 2JG', 'Canada', '204-854-5432'),
(4,'Himanshu', 'Srivastava', '190 Tarunshakti Society', 'Ahmedabad', '380061', 'India', '9112524578'),
(5,'Anupma', 'Prakash', '63 Defence Colony', 'Lucknow', '123547', 'India', '0878454511'),
(6,'Sara', 'Srivastava', '44 Court Colony', 'J & K', '321321', 'India', '9879741254'),
(7,'Ankur', 'Kulshrestha', '2252 Keystone', 'Washington', '9874 21547', 'U.S.', '457-123-5432'),
(8,'Sakshi', 'Kulshrestha', '2252 Redmond', 'Washington', '9874 21547', 'U.S.', '457-323-2132'),
(9,'Ayansh', 'Kulshrestha', '2252 Leusite', 'Washington', '9874 21547', 'U.S.', '457-111-1111');

INSERT INTO emails
(user_id,email)
VALUES
(1,'anu@example.com'),
(1, 'anu@hotmail.com'),
(2, 'ankit@example.com'),
(2, 'ankit@gmail.com'),
(3, 'anish@gmail.com'),
(3, 'anish@example.com'),
(4, 'him@gmail.com'),
(4, 'himanshu@example.com'),
(5, 'anupma@gmail.com'),
(5, 'anupmap@example.com'),
(6, 'sara@gmail.com'),
(6, 'sara@example.com'),
(7, 'ankur@gmail.com'),
(7, 'ankur@example.com'),
(8, 'sakshi@gmail.com'),
(8, 'sakshi@example.com'),
(9, 'ayansh@gmail.com'),
(9, 'ayansh@example.com');