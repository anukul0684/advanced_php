# Advanced PHP Assignment 1

## Started Files

These are the starter files for Advanced PHP Assignment 1.  

* `booksite.sqlite.sql` - raw sql for the sqlite database, in case you need to study its structure or re-create it for some reason.
* `database.sqlite` - sqlite database for the booksite, ready to use
* `index.html` - to be used for the Vanilla Ajax version of assignment 1
* `index2.html` - to be user for the library (jquery, axios, fetch) version of assignment 1
* `README.md` - this file
* `server.php` - to be used as the api for your database... all your PHP code should be in this file

CREATE TABLE author (                                                                                                                                                    
  author_id INTEGER NOT NULL PRIMARY KEY,                                                                                                                                
  name varchar(255) DEFAULT NULL,                                                                                                                                        
  country varchar(255) DEFAULT NULL                                                                                                                                      
                                                                                                                                                                         
);                                                                                                                                                                       
CREATE TABLE book (                                                                                                                                                      
  book_id INTEGER NOT NULL PRIMARY KEY,                                                                                                                                  
  title varchar(255) DEFAULT NULL,                                                                                                                                       
  year_published INTEGER DEFAULT NULL,                                                                                                                                   
  num_pages INTEGER DEFAULT NULL,                                                                                                                                        
  in_print tinyint(1) DEFAULT NULL,                                                                                                                                      
  price decimal(5,2) DEFAULT NULL,                                                                                                                                       
  description text,                                                                                                                                                      
  image varchar(255) DEFAULT NULL,                                                                                                                                       
  author_id INTEGER DEFAULT NULL,                                                                                                                                        
  publisher_id INTEGER DEFAULT NULL,                                                                                                                                     
  format_id INTEGER DEFAULT NULL,                                                                                                                                        
  genre_id INTEGER DEFAULT NULL                                                                                                                                          
);                                                                                                                                                                       
CREATE TABLE format (                                                                                                                                                    
  format_id INTEGER NOT NULL PRIMARY KEY,                                                                                                                                
  name varchar(255) DEFAULT NULL                                                                                                                                         
);                                                                                                                                                                       
CREATE TABLE genre (                                                                                                                                                     
  genre_id INTEGER NOT NULL PRIMARY KEY,                                                                                                                                 
  name varchar(255) DEFAULT NULL                                                                                                                                         
);                                                                                                                                                                       
CREATE TABLE publisher (                                                                                                                                                 
  publisher_id INTEGER NOT NULL PRIMARY KEY,                                                                                                                             
  name varchar(255) DEFAULT NULL,                                                                                                                                        
  city varchar(255) DEFAULT NULL,                                                                                                                                        
  phone varchar(255) DEFAULT NULL                                                                                                                                        
);   


