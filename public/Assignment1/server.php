<?php

//main function for responding to XMLHttpRequest or fetch jQuery function
/**
 * [main function created to echo the data as response to XMLHttpRequest object]
 
 */
function main()
{
    //writing code in a try{} - catch{} block for handling exceptions
    try
    {
        //check if the request is GET and if search content is sent with url
        if('GET' === $_SERVER['REQUEST_METHOD'] && !empty($_GET['search_content'])) {
            //call the search function and assign data to a variable
            $result = getSearchItems($_GET['search_content']);
        } 
        //check if the request is GET and if book id is sent with url
        elseif('GET' === $_SERVER['REQUEST_METHOD'] && !empty($_GET['book_id'])) {
            //call the detail function and assign data to a variable
            $result = getDetail($_GET['book_id']);
        } 
        //check if the request is GET and if $_GET global variable is empty
        elseif('GET' === $_SERVER['REQUEST_METHOD'] && empty($_GET)) {
            //call the list function and assign data to a variable
            $result = getList();            
        }

        //echo the variable after encoding to json
        echo json_encode($result);

    } catch(Exception $e) { //catch any exception if found in try block
        echo json_encode($e->getTrace()); //echo the error as response to XMLHttpRequest object
    }
}

//Functions used to fetch the data in the index1 and index2 html files

/**
 * [initDBH function to create a PDO object connecting to database.sqlite file]
 * @return [PDO] [object of PDO created with error and fetch mode attribute]
 */
function initDBH()
{
    $dbh = new PDO('sqlite:database.sqlite');
    $dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $dbh;
}

/**
 * [getList gets list of book name, its image, year it was published,
 * author's name and book's genre name]
 * thumbnail of image
 * title
 * author
 * genre
 * year
 * @return array [all records joining three tables book, author and genre]
 */
function getList()
{
    $conn = initDBH();
    $query = "SELECT b.book_id as book_id,
                    b.title as title,
                    b.year_published as year,
                    IFNULL(b.image,'default.jpg') as image, 
                    a.name as author,
                    g.name as genre
                    from 
                    book b
                    Join author a on a.author_id = b.author_id
                    Join genre g on g.genre_id = b.genre_id
                    order by b.title asc";
    $stmt = $conn->prepare($query);
    $stmt->execute();
    return $stmt->fetchAll();
}

/**
 * [getSearchItems gets all the matching records for title, author, genre, publisher and format]
 * @param  [string] $content [string entered by user]
 * @return [array]          [list of records searched for the user input]
 */
function getSearchItems($content)
{

    $dbh = initDBH();
    $query = "SELECT b.book_id as book_id,
                        b.title as title,
                        b.year_published as year,
                        IFNULL(b.image,'default.jpg') as image, 
                        a.name as author,
                        g.name as genre
                        from 
                        book b
                        Join author a on a.author_id = b.author_id
                        Join genre g on g.genre_id = b.genre_id
                        Join publisher p on p.publisher_id = b.publisher_id
                        Join format f on f.format_id = b.format_id
                        where (b.title like '%' || :content || '%'
                        or a.name like '%' || :content || '%'
                        or g.name like '%' || :content || '%')
                        order by b.title asc";
    //$stmt->bindValue(':content',(string)('%'. $content . '%'),PDO::PARAM_STR);
    $params = array(
        ':content' => $content
    );

    $stmt = $dbh->prepare($query);

    $stmt->execute($params);

    return $stmt->fetchAll();     
}

/**
 * [getDetail function to get all details of a book by joining 5 tables]
 * @param  [int] $id [book id]
 * @return [array]     [details of a book along with its author, 
 * publisher, genre, and format details]
 */
function getDetail($id)
{
    $dbh = initDBH();
    $query = "SELECT b.title as title,
                    b.year_published as year,
                    IFNULL(b.image,'default.jpg') as image,
                    b.num_pages as num_pages,
                    '$' || b.price as price,
                    b.description as description,
                    case when b.in_print = 1 then 'YES'
                    else 'NO' end as `in_print`, 
                    IFNULL(a.name,'default') as author,
                    a.country as country,
                    g.name as genre,
                    IFNULL(p.name,'default') as publisher,
                    p.city as city,
                    p.phone as phone,
                    f.name as format
                    from 
                    book b
                    Join author a on a.author_id = b.author_id
                    Join genre g on g.genre_id = b.genre_id
                    Join publisher p on p.publisher_id = b.publisher_id
                    Join format f on f.format_id = b.format_id
                    where b.book_id = :book_id";
    $params = array(
                'book_id'=>$id
            );
    $stmt = $dbh->prepare($query);
    $stmt->execute($params);
    return $stmt->fetch();
}

//call the main() function to execute and echo the response for XMLHttpRequest object
main();